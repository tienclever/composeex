package com.example.compose.presentation.avata.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.compose.data.database.model.Avatar
import com.example.compose.presentation.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AvatarViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val _avatarLiveData = MutableStateFlow(emptyList<Avatar>())
    val avatarLiveData: StateFlow<List<Avatar>> get() = _avatarLiveData

    private val _avatarCurrentLiveData = MutableStateFlow(Avatar("","","",""))
    val avatarCurrentLiveData: StateFlow<Avatar> get() = _avatarCurrentLiveData

    private val _loading = MutableStateFlow(false)
    val loading: StateFlow<Boolean> get() = _loading

    init {
        loadAvatar()
    }

    private fun loadAvatar() {
        viewModelScope.launch(IO) {
            _loading.emit(true)
            repository.getAvatars().collect {
                try {
                    it.let { response ->
                        _avatarLiveData.emit(response)
                        _loading.emit(false)
                    }
                } catch (ex: Exception) {
                    _loading.emit(false)
                }
            }
        }
    }

    fun getCurrentAvatar(name: String){
        viewModelScope.launch(IO) {
            repository.getCurrentAvatar(name).collect {
                _avatarCurrentLiveData.emit(it)
            }
        }
    }
}