package com.example.compose.presentation.avata

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.rememberAsyncImagePainter
import com.example.compose.data.database.model.Avatar
import com.example.compose.presentation.avata.viewmodel.AvatarViewModel
import com.example.compose.theme.CustomTheme
import com.example.compose.theme.black_90
import com.example.compose.theme.blue
import com.example.compose.theme.blue_1
import com.example.compose.util.CustomDrawable
import com.example.compose.util.NavRoutes
import com.google.accompanist.drawablepainter.rememberDrawablePainter
import dagger.hilt.android.AndroidEntryPoint
import me.onebone.toolbar.CollapsingToolbarScaffold
import me.onebone.toolbar.ScrollStrategy
import me.onebone.toolbar.rememberCollapsingToolbarScaffoldState

private const val TAG = "AvatarActivity"

@AndroidEntryPoint
class AvatarActivity : AppCompatActivity() {
    private val avatarViewModel: AvatarViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            CustomTheme(darkTheme = false) {
                Scaffold {
                    NavigationComponent(navController)
                }
            }
        }
    }

    @Composable
    fun NavigationComponent(navController: NavHostController) {
        NavHost(
            navController = navController,
            startDestination = NavRoutes.Home.route + "/{name}"
        ) {
            composable(
                NavRoutes.Home.route + "/{name}",
                arguments = listOf(navArgument("name") { type = NavType.StringType })
            ) {

                ListAvatarScreen {
                    navController.navigate(NavRoutes.Detail.route + "/$it")
                }
            }
            composable(NavRoutes.Detail.route + "/{name}") { backStackEntry ->
                DetailScreen(backStackEntry.arguments?.getString("name"), avatarViewModel)
            }
        }
    }

    @Composable
    fun ListAvatarScreen(onclickItem: (String) -> Unit) {
        val state = rememberCollapsingToolbarScaffoldState()

        val customDrawable = CustomDrawable()


        CollapsingToolbarScaffold(
            modifier = Modifier.fillMaxSize(),
            state = state,
            scrollStrategy = ScrollStrategy.ExitUntilCollapsed,
            toolbar = {
                val textSize = (18 + (30 - 18) * state.toolbarState.progress).sp
                val imgSize = (30 + (100 - 30) * state.toolbarState.progress).dp
                Box(
                    modifier = Modifier
                        .background(black_90)
                        .fillMaxWidth()
                        .height(150.dp)
                        .pin()
                )

                Text(
                    text = "Compose demo",
                    modifier = Modifier
                        .road(Alignment.CenterStart, Alignment.BottomEnd)
                        .padding(60.dp, 16.dp, 16.dp, 16.dp),
                    color = Color.White,
                    fontSize = textSize
                )
                Image(
                    modifier = Modifier
                        .pin()
                        .padding(16.dp)
                        .size(imgSize)
                        .clickable {
                            onBackPressed()
                        },
                    painter = rememberDrawablePainter(drawable = customDrawable),
                    contentDescription = null
                )
            }
        ) {
            Box {
                setListAvatar(avatarViewModel = avatarViewModel) {
                    onclickItem(it)
                }
                setLoading(avatarViewModel = avatarViewModel)
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun DetailScreen(name: String?, avatarViewModel: AvatarViewModel) {
    name?.let {
        avatarViewModel.getCurrentAvatar(name = name)

        val avatar by avatarViewModel.avatarCurrentLiveData.collectAsState()

        val scrollState = rememberScrollState()
        Column(modifier = Modifier.padding(top = 0.dp, bottom = 20.dp)) {
            Card(shape = RoundedCornerShape(0.dp, 0.dp, 10.dp, 10.dp)) {
                Image(
                    painter = rememberAsyncImagePainter(avatar.imageUrl),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .fillMaxHeight(0.3f)
                        .blur(3.dp),
                )
            }

            Column(
                modifier = Modifier
                    .verticalScroll(
                        state = scrollState,
                        enabled = true
                    )
            ) {
                Text(
                    text = avatar.name,
                    color = blue_1,
                    fontSize = 20.sp,
                    style = MaterialTheme.typography.h3,
                    modifier = Modifier.padding(5.dp)
                )
                Text(text = avatar.bio,
                    fontSize = 13.sp,
                    style = MaterialTheme.typography.h3,
                    modifier = Modifier.padding(5.dp))
                Text(text = avatar.team, color = blue,
                    fontSize = 14.sp,
                    style = MaterialTheme.typography.h3,
                    modifier = Modifier.padding(5.dp))
                Text(text = avatar.imageUrl, color = blue,
                    fontSize = 14.sp,
                    style = MaterialTheme.typography.h3,
                    modifier = Modifier.padding(5.dp))
            }
        }
    }
}

@Composable
fun setLoading(avatarViewModel: AvatarViewModel) {
    val loadingProgressBar = avatarViewModel.loading.collectAsState(true)
    if (loadingProgressBar.value) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator()
        }
    }
}

@SuppressLint("ComposableNaming")
@Composable
fun setListAvatar(avatarViewModel: AvatarViewModel, onclickItem: (String) -> Unit) {
    val avatars = avatarViewModel.avatarLiveData.collectAsState(emptyList())
    val listState = rememberLazyListState()

    LazyColumn(state = listState) {
        items(avatars.value) { avatar ->
            rowItem(modifier = Modifier.padding(8.dp), avatar) {
                onclickItem(avatar.name)
            }
            Divider(startIndent = 10.dp)
        }
    }
}

@SuppressLint("ComposableNaming")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun rowItem(modifier: Modifier, avatar: Avatar, onclickItem: () -> Unit) {
    ListItem(
        modifier = modifier
            .clickable {
                onclickItem()
            }
            .padding(vertical = 8.dp),
        icon = {
            Card(shape = CircleShape) {
                Image(
                    painter = rememberAsyncImagePainter(avatar.imageUrl),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.size(88.dp)
                )
            }
        },
        text = {
            Text(text = avatar.name, color = blue)
        },
        secondaryText = {
            Text(text = avatar.name)
        }
    )
}

