package com.example.compose.presentation.repository

import com.example.compose.data.database.model.Avatar
import kotlinx.coroutines.flow.Flow

interface Repository {
    suspend fun getAvatars(): Flow<List<Avatar>>

    suspend fun getCurrentAvatar(name: String): Flow<Avatar>
}