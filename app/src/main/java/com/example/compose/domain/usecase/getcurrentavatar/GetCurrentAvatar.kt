package com.example.compose.domain.usecase.getcurrentavatar

import com.example.compose.data.database.model.Avatar
import com.example.compose.presentation.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class getCurrentAvatar @Inject constructor(
    private val repository: Repository
) {
    suspend operator fun invoke(name: String): Flow<Avatar> = repository.getCurrentAvatar(name = name)
}