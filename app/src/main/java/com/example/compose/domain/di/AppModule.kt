package com.example.compose.domain.di

import com.example.compose.data.database.Dao
import com.example.compose.data.remote.ApiService
import com.example.compose.data.repository.RepositoryImpl
import com.example.compose.presentation.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRepository(apiService: ApiService, dao: Dao): Repository {
        return RepositoryImpl(apiService = apiService, dao = dao)
    }
}