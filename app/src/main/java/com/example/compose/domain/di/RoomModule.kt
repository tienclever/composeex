package com.example.compose.domain.di

import android.content.Context
import androidx.room.Room
import com.example.compose.data.database.AvatarDatabase
import com.example.compose.data.database.Dao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun provideDB(@ApplicationContext context: Context): AvatarDatabase {
        return Room.databaseBuilder(
            context,
            AvatarDatabase::class.java,
            AvatarDatabase.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun provideAvatarDao(avatarDatabase: AvatarDatabase): Dao {
        return avatarDatabase.getAvatarDao()
    }
}