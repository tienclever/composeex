package com.example.compose.domain.usecase.getavatars

import com.example.compose.data.database.model.Avatar
import com.example.compose.presentation.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class getAvatar @Inject constructor(
    private val repository: Repository
) {
    suspend operator fun invoke(): Flow<List<Avatar>> = repository.getAvatars()
}