package com.example.compose.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Avatar(
    @SerializedName("team")
    val team: String,
    @SerializedName("bio")
    @PrimaryKey
    val bio: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("imageUrl")
    val imageUrl: String
): Serializable