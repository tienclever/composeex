package com.example.compose.data.remote.model

import com.example.compose.data.database.model.Avatar
import com.google.gson.annotations.SerializedName

data class AvatarDto(
    val team: String,
    val bio: String,
    @SerializedName("createdby")
    val createdBy: String,
    @SerializedName("firstappearance")
    val firstAppearance: String,
    @SerializedName("imageurl")
    val imageUrl: String,
    val name: String,
    val publisher: String,
    @SerializedName("realname")
    val realName: String
)

fun AvatarDto.toAvatar(): Avatar {
    return Avatar(
        team = team,
        bio = bio,
        name = name,
        imageUrl = imageUrl,
    )
}