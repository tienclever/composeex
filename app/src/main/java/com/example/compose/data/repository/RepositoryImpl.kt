package com.example.compose.data.repository

import com.example.compose.data.database.Dao
import com.example.compose.data.database.model.Avatar
import com.example.compose.data.remote.ApiService
import com.example.compose.data.remote.model.toAvatar
import com.example.compose.presentation.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val dao: Dao
): Repository {

    override suspend fun getAvatars(): Flow<List<Avatar>> {
        return getDataRemote().flatMapLatest {
            getDataLocal()
        }.flowOn(Dispatchers.IO)
    }

    private suspend fun getDataRemote() = flow {
        val listAvatar = apiService.getAvatars().map {
            dao.insertItem(it.toAvatar())
        }
        emit(listAvatar)
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)

    private suspend fun getDataLocal(): Flow<List<Avatar>> = flow {
        emit(dao.getAll() ?: emptyList())
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)

    override suspend fun getCurrentAvatar(name: String): Flow<Avatar> = flow{
        emit(dao.getAvatarByName(name))
    }
}