package com.example.compose.data.remote

import com.example.compose.data.remote.model.AvatarDto
import retrofit2.http.GET

interface ApiService {
    @GET("marvel")
    suspend fun getAvatars(): List<AvatarDto>
}