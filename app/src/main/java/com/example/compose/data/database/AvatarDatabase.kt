package com.example.compose.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.compose.data.database.model.Avatar

@Database(entities = [Avatar::class], version = 1)
abstract class AvatarDatabase : RoomDatabase() {
    companion object {
        const val DATABASE_NAME = "avatar.db"
    }

    abstract fun getAvatarDao(): Dao
}