package com.example.compose.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.compose.data.database.model.Avatar

@Dao
interface Dao {
    @Query("SELECT * FROM avatar")
    fun getAll(): List<Avatar>

    @Query("SELECT * FROM avatar WHERE name =:name")
    fun getAvatarByName(name: String): Avatar

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg users: Avatar)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(avatar: Avatar)

    @Delete
    fun delete(avatar: Avatar)
}