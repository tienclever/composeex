package com.example.compose.util

sealed class NavRoutes(val route: String) {
    object Home : NavRoutes("listAvatar")
    object Detail : NavRoutes("detail")
}