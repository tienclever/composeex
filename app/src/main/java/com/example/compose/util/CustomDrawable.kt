package com.example.compose.util

import android.animation.ValueAnimator
import android.graphics.*
import android.graphics.drawable.Drawable


class CustomDrawable: Drawable(){

    private val redPaint: Paint = Paint().apply {
        setARGB(233, 41, 9, 234)
    }

    private val redPaint1: Paint = Paint().apply {
        setARGB(113, 222, 222, 0)
    }

    override fun draw(canvas: Canvas) {
        val width = bounds.width()
        val height = bounds.height()

        val radius: Float = width.coerceAtMost(height).toFloat() / 2f
        val radius1: Float = width.coerceAtMost(height/2).toFloat() / 2f

        canvas.drawCircle((width/2).toFloat(), (height/2).toFloat(), radius, redPaint)
        canvas.drawCircle((width/2).toFloat(), (height/2).toFloat(), radius1, redPaint1)
    }

    override fun setAlpha(p0: Int) {
    }

    override fun setColorFilter(p0: ColorFilter?) {
    }

    override fun getOpacity(): Int {
        //Phải là PixelFormat.UNKNOWN, TRANSLUCENT, TRANSPARENT hoặc OPAQUE
        return PixelFormat.TRANSPARENT
    }
}