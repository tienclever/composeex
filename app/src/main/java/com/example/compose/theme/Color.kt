package com.example.compose.theme

import androidx.compose.ui.graphics.Color

val Red200 = Color(0xfff297a2)
val Red300 = Color(0xffea6d7e)
val Red700 = Color(0xffdd0d3c)
val Red800 = Color(0xffd00036)
val Red900 = Color(0xffc20029)
val black_90 = Color(0xE60C0C0C)
val blue_1 = Color(0xFF9C27B0)
val blue = Color(0xFF00BCD4)
